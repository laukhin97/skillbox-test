import json
import urllib.request
from math import radians, cos, sin, asin, sqrt

SVO_LONGITUDE = 37.414589
SVO_LATITUDE = 55.972642
EARTH_RADIUS = 6371


class Aircraft:
    def __init__(self, aircraft):
        self.name = aircraft[1]
        self.country = aircraft[2]
        self.longitude = aircraft[5]
        self.latitude = aircraft[6]

    @property
    def distance(self):
        return haversine(self.longitude, self.latitude, SVO_LONGITUDE, SVO_LATITUDE)


def haversine(longitude1, latitude1, longitude2, latitude2):
    """
    Считает расстояние от одной географической точки до другой.
    :param longitude1: долгота в десятичных градусах.
    :param latitude1: широта в десятичных градусах.
    :param longitude2: долгота в десятичных градусах.
    :param latitude2: широта в десятичных градусах.
    :return: расстояние в километрах.
    """
    longitude1, latitude1, longitude2, latitude2 = map(radians, [longitude1, latitude1, longitude2, latitude2])
    delta_longitude = longitude2 - longitude1
    delta_latitude = latitude2 - latitude1
    a = sin(delta_latitude/2)**2 + cos(latitude1) * cos(latitude2) * sin(delta_longitude/2)**2
    c = 2 * asin(sqrt(a))
    distance = EARTH_RADIUS * c
    return distance


if __name__  == '__main__':
    json_data = urllib.request.urlopen("https://opensky-network.org/api/states/all").read().decode('utf-8')
    aircrafts = [Aircraft(record) for record in json.loads(json_data)['states']]
    not_null_aircrafts = filter(lambda x: (x.longitude is not None) and (x.latitude is not None), aircrafts)
    aircrafts_near_svo = [aircraft for aircraft in not_null_aircrafts if aircraft.distance <= 100]
    for aircraft in aircrafts_near_svo:
        print(f"{aircraft.name.rstrip()} from {aircraft.country} is on {int(aircraft.distance)} km from SVO")